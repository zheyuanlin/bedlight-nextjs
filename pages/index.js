import Head from 'next/head'
import css from './index.module.scss';

const Logo = () => {
    return (
        <svg width="64" height="18" viewBox="0 0 64 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="9" cy="9" r="9" fill="#50C5DC"/>
            <circle cx="55" cy="9" r="9" fill="#A21A68"/>
            <circle cx="32" cy="9" r="9" fill="#FCB415"/>
        </svg>
    )
};

const Home = () => (
  <div className={css.overallContainer}>
    <Head>
      <html lang={"en"} />
      <title>BedLight</title>
      <link rel="icon" href="/favicon.ico" />
        <script dangerouslySetInnerHTML={{
            __html: `
                (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-N3X9W9B');
            `
        }} />
    </Head>
      <noscript dangerouslySetInnerHTML={{
          __html: `<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N3X9W9B"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe>`
      }} />
    <main className={css.mainContainer}>
      <div className={css.logo}>
        <Logo />
      </div>
      <a className={css.goToAppButton} href="https://app.bedlight.io/">
        open web app
      </a>
      <h1 className={css.title}>
          An anonymous app for easy sexual communication with your partner
      </h1>
      <p className={css.description}>
          Asking for sex can be hard. Sometimes it's not just a yes/no answer...
      </p>
      <p className={css.descriptionThick}>
          Get access to a simple 5-point scale
          that covers almost all situations and lets you easily communicate with your partner about sex.
      </p>

      <p className={css.smallText}>
            <span>
            (inspired by&nbsp;
                <a className={css.blueText}
                   href="https://www.reddit.com/r/sex/comments/anrxgo/life_hack_for_initiating_sex_with_your_partner_a/"
                >
              reddit
            </a>
                &nbsp;and sex therapy practices)
        </span>
      </p>

      <hr className={css.divider} />

      <h2 className={css.title}>
          How it works
      </h2>

        <p className={css.description}>
            Step 1: create an account and log in, no email required!
        </p>
        <img alt="signup gif image" className={css.video} src={'/signup.gif'} />

        <p className={css.description}>
            Step 2: let your partner know if you are in the mood for sex today
        </p>
        <img alt="button click gif image" className={css.video} src={'/selectHorniness.gif'} />

        <p className={css.description}>
            Step 3: your partner can see your choice anytime by visiting your link, and you can also see their’s by visiting their link
        </p>
        <p className={css.smallText}>
            links are always in the format of <span className={css.blueText}>app.bedlight.io/[your username]</span>
        </p>
        <img alt="highlight link gif image" className={css.video} src={'/highlightLink.gif'} />
        <img alt="open link in browser gif image" className={css.video} src={'/visitLink.gif'} />

        <p className={css.description}>
            Step 4: if you set a high number, just wait for your partner to pursue you and initiate 🔥
        </p>

      <a className={css.getStartedButton} href="https://app.bedlight.io/">
        <h3>Get Started &rarr;</h3>
      </a>
    </main>

    <footer>
    </footer>
  </div>
);

export default Home
